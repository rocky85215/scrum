# 下週進度規劃
+ VPN 連線測試 雅涵 俊杰
+ 支援子計畫二 OPCUA client & HTTPs request 測試完成 昀星 盈媗
+ (5G計畫)場域監控的直播串流服務 子誼
+ 場域邊緣部屬維護 梓豪 益均
+ (5G計畫)雷射頁面維護與串接 俊杰 子多
+ 5G與資安Demo影片錄製與規劃 我跟子誼
# Story
+ 會議紀錄文件分類處理 `sprint 4`
+ k8s high-available topology `sprint 4`
+ k8s federated learning in high-available topology architecture `sprint 4`
+ 在gitlab上做每個禮拜的story維護與往前推進 `sprint 4`
+ (資安計畫)"OPCUA&HTTPS by oidc system" client gateway交接给子計畫二 `sprint 4`
+ (5G計畫)場域監控的直播串流服務 (不用特別編碼 僅需功能) `sprint 4`
+ paper sharing `sprint 4`
    1. k8s
        + 盈媗 雅涵
    2. AI
        + 梓豪 昀星 子多 益均
    3. 韌體
        + 子誼
+ 支援子計畫二 OPCUA client & HTTP request 測試完成 `sprint 4`
+ VPN 連線測試 `sprint 4`
# Sprint backlog
+ 自動化解決方案與Flask授權系統的整合
+ 5G k8s框架與總計畫框架的系統架構整合
+ 與安華資安輔導後的文件撰寫與需求開發
+ OPCUA server and client to k8s
+ MongoDB Ops Manager(MongoDB在k8s上的特別監控GUI)
+ VVC streaming 研發
+ openvpn server
+ 資安計畫書 - IDS IPS
+ 編碼 找越南博士生處裡
+ 會議紀錄文件分類處理 `sprint 4`
+ k8s high-available topology `sprint 4`
+ k8s federated learning in high-available topology architecture `sprint 4`
+ 在gitlab上做每個禮拜的story維護與往前推進 `sprint 4`
+ (資安計畫)"OPCUA&HTTPS by oidc system" client gateway交接给子計畫二 `sprint 4`
+ (5G計畫)場域監控的直播串流服務 (不用特別編碼 僅需功能) `sprint 4`
+ paper sharing `sprint 4`
    1. k8s
        + 盈媗 雅涵
    2. AI
        + 梓豪 昀星 子多 益均
    3. 韌體
        + 子誼
+ 支援子計畫二 OPCUA client & HTTP request 測試完成 `sprint 4`
+ VPN 連線測試 `sprint 4`
# Product backlog
+ VVC視訊壓縮功能 (use vvenc) `sprint 1`
+ 在k8s上展示介面的基礎建設 `sprint 1` `sprint 2`
+ 在k8s上授權管理系統的基礎建設 `sprint 1` `sprint 2`
+ 在k8s上資料庫系統的基礎建設 `sprint 1` `sprint 2`
+ 基礎建設的整合串接 [use k8s service(load balancing and DNS) and ingress(nginx back proxy and load balancing) and port-forwarding(from VM to physical machine)] `sprint 2`
+ k8s中央監控系統的基礎建設 `(+)sprint 2`
+ k8s 資安計畫 服務自動修復功能 (use k8s liveness) `(+)sprint 2`
+ IEC 62443 差異性分析文件蒐集與撰寫 `sprint 3`
+ 在gitlab上做每個禮拜的story維護與往前推進 `sprint 3`
+ 在k8s上聯邦式學習的基礎建設 `(+)sprint 3`
+ 使用手冊文件撰寫 (in gitlab) `(+)sprint 3`
+ k8s 5G計畫 封包排程 `(-)sprint 3`
# Retrospective
|期程|時間|story point num.|story交付完成率(%)|工作時間統計(hour)|估計工作量|story範圍變更次數|
|-|-|-|-|-|-|-|
|sprint 1|2022/02/14~2022/03/04|4|25%|子誼 - 30hr<br>盈媗 - 22hr<br>梓豪 - 28hr<br>俊杰 - 25hr<br>雅涵 - 23hr|every one in 8 hour|0|
|sprint 2|2022/03/07~2022/04/01|4|150%<br>(new and finish)<br>1. k8s中央監控系統的基礎建設<br>2. k8s 資安計畫 服務自動修復功能 (use k8s liveness)|子誼 - 40hr<br>盈媗 - 40hr<br>梓豪 - 42hr<br>益均 - 54hr<br>俊杰 - 40hr<br>雅涵 - 48hr|every one in 48 hour|(+)<br>1. k8s中央監控系統的基礎建設<br>2. k8s 資安計畫 服務自動修復功能 (use k8s liveness)|
|sprint 3|2022/04/06~2022/04/29|3|133%<br>(new and finish)<br>1. 在k8s上聯邦式學習的基礎建設<br>2. 使用手冊文件撰寫 (in gitlab)<br>(remove)<br>1. k8s 5G計畫 封包排程|子誼 - 36hr<br>盈媗 - 15hr<br>梓豪 - 34hr<br>益均 - 20hr<br>俊杰 - 20hr<br>雅涵 - 24hr|every one in 48 hour|(+)<br>1. 在k8s上聯邦式學習的基礎建設<br>2. 使用手冊文件撰寫 (in gitlab)<br>(-)<br>1. k8s 5G計畫 封包排程|
|sprint 4|2022/05/02~2022/05/27|9|None|None|every one in 48 hour|None|
